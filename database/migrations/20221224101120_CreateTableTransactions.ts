import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  return knex.raw(`
    CREATE TABLE transactions (
        transaction_id SERIAL PRIMARY KEY,
        chart_account_id UUID NOT NULL,
        reference VARCHAR(255) NOT NULL,      
        amount DECIMAL(10, 2),
        created_at TIMESTAMPTZ DEFAULT NOW(),
        CONSTRAINT fk_operation_chart_account
            FOREIGN KEY (chart_account_id)
                REFERENCES chart_accounts (chart_account_id)
            ON UPDATE CASCADE
            ON DELETE CASCADE
        
    );`);
}

export async function down(knex: Knex): Promise<void> {
  return knex.raw("DROP TABLE transactions;");
}
