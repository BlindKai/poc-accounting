import { PinoLoggerOptions } from "fastify/types/logger";

export const loggerConfigs: Record<
  typeof process.env.NODE_ENV,
  PinoLoggerOptions
> = {
  test: { enabled: false },
  production: { enabled: true },
  development: {
    enabled: true,
    transport: {
      target: "pino-pretty",
      options: {
        ignore: "pid,hostname",
        translateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'",
      },
    },
  },
};
