import Fastify from "fastify";
import { db } from "../database";
import { chartAccountRoutes } from "./domains/chart-accounts/chart-accounts.routes";
import { transactionRoutes } from "./domains/transactions/transactions.routes";
import { loggerConfigs } from "./logger/logger.config";

export const application = Fastify({
  logger: loggerConfigs[process.env.NODE_ENV] ?? true,
}).decorate("db", db);

application.register(chartAccountRoutes, { prefix: "accounts" });
application.register(transactionRoutes, { prefix: "transactions" });

application.get("/favicon.ico", async function (request, reply) {
  return reply.status(204);
});

application.get("/", async function (request, reply) {
  reply.header("Content-Type", "html");
  return reply.send(`<h1>Hello World</h1>`);
});
