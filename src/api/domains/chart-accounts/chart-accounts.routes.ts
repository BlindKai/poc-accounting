import { FastifyInstance, FastifyPluginAsync } from "fastify";
import { ChartAccountsService } from "./chart-accounts.service";

export const chartAccountRoutes: FastifyPluginAsync = async function (
  app: FastifyInstance & { chartAccountsService: ChartAccountsService }
) {
  app.decorate(
    "chartAccountsService",
    new ChartAccountsService(app.db, app.log)
  );

  app.get("/", async function (request, reply) {
    return await app.chartAccountsService.select();
  });

  app.post<{ Body: { name: string; startingBalance: number } }>(
    "/",
    async ({ body }, reply) => {
      const account = await app.chartAccountsService.insert(
        body.name,
        body.startingBalance
      );
      return reply.status(201).send(account);
    }
  );

  app.delete<{ Params: { id: string } }>("/:id", async (request, reply) => {
    return await app.chartAccountsService.destroy(request.params.id);
  });
};
