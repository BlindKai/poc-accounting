import { FastifyBaseLogger } from "fastify";
import { Knex } from "knex";

export class ChartAccountsService {
  constructor(
    private readonly db: Knex,
    private readonly logger: FastifyBaseLogger
  ) {}

  async select() {
    const { rows } = await this.db.raw(
      'SELECT *, "balance"::float FROM chart_accounts;'
    );

    return rows;
  }

  async insert(name: string, startingBalance: number) {
    const { rows } = await this.db.raw(
      'INSERT INTO chart_accounts ("name", "balance") VALUES (:name, :startingBalance) RETURNING *, "balance"::float;',
      { name, startingBalance }
    );

    return rows[0];
  }

  async destroy(id: string) {
    const { rows } = await this.db.raw(
      'DELETE FROM chart_accounts WHERE chart_account_id = :id RETURNING *, "balance"::float;',
      { id }
    );

    return rows[0];
  }
}
