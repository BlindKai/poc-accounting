import { FastifyInstance, FastifyPluginAsync } from "fastify";
import { TransactionsService } from "./transactions.service";

export const transactionRoutes: FastifyPluginAsync = async function (
  app: FastifyInstance & { transactionsService: TransactionsService }
) {
  app.decorate("transactionsService", new TransactionsService(app.db, app.log));

  app.get<{ Querystring: { account: string } }>("/", async (request, reply) => {
    const accounts = await app.transactionsService.select(
      request.query.account
    );
    return reply.send(accounts);
  });

  app.post<{
    Body: {
      chartAccountId: string;
      reference: string;
      amount: number;
    };
  }>("/", async ({ body }, reply) => {
    const operation = await app.transactionsService.insert(
      body.chartAccountId,
      body.reference,
      body.amount
    );

    return reply.status(201).send(operation);
  });
};
