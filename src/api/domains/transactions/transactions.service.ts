import { FastifyBaseLogger } from "fastify";
import { Knex } from "knex";

export class TransactionsService {
  constructor(
    private readonly db: Knex,
    private readonly logger: FastifyBaseLogger
  ) {}

  async select(chart_account_id: string) {
    const { rows } = await this.db.raw(
      "SELECT *, amount::float FROM transactions WHERE chart_account_id = :chart_account_id ORDER BY created_at DESC;",
      { chart_account_id }
    );
    return rows;
  }

  async insert(chart_account_id: string, reference: string, amount: number) {
    const transaction = await this.db.transaction();

    try {
      const { rows: chartAccounts } = await transaction.raw(
        'SELECT "balance"::float FROM "chart_accounts" WHERE chart_account_id = ? LIMIT 1;',
        chart_account_id
      );

      const currentBalance = chartAccounts[0].balance;

      await transaction.raw(
        `INSERT INTO "transactions" ("chart_account_id", "reference", "amount") 
         VALUES (:chart_account_id, :reference, :amount)
         RETURNING *;`,
        { chart_account_id, reference, amount }
      );

      await transaction.raw(
        'UPDATE "chart_accounts" SET updated_at = NOW(), balance = :newBalance WHERE chart_account_id = :chart_account_id',
        { chart_account_id, newBalance: currentBalance + amount }
      );

      transaction.commit();
    } catch (error) {
      transaction.rollback();
      throw error;
    }
  }
}
