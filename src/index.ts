import "dotenv/config";
import { application } from "./api/server";

const main = async () => {
  try {
    await application.listen({
      host: process.env.HOST,
      port: +process.env.PORT,
    });
  } catch (error) {
    application.log.error(error);
    process.exit(1);
  }
};

main();
