import * as path from "path";

import * as dotenv from "dotenv";
dotenv.config({ path: path.join(__dirname, "..", "..", ".env") });

import { Knex } from "knex";

const { DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD } = process.env;

const knexConfig: Knex.Config = {
  client: "postgres",
  pool: { min: 0, max: 10 },
  acquireConnectionTimeout: 10000,
  connection: {
    host: DB_HOST,
    port: +DB_PORT,
    database: DB_DATABASE,
    user: DB_USERNAME,
    password: DB_PASSWORD,
  },
  migrations: { directory: "../../database/migrations", extension: ".ts" },
  seeds: { directory: "../../database/seeds", extension: ".ts" },
  debug: true,
};

export default knexConfig;
